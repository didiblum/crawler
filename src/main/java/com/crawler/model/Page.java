package com.crawler.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Page implements Serializable {
    private String url;
    private List<Page> subPages = new ArrayList<>();
    private boolean triedToFetchSubPages = false;
    private double ratio;

    public Page(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public List<Page> getSubPages() {
        return subPages;
    }

    public void setSubPages(List<Page> subPages) {
        this.subPages = subPages;
    }

    public boolean isTriedToFetchSubPages() {
        return triedToFetchSubPages;
    }

    public void setTriedToFetchSubPages(boolean triedToFetchSubPages) {
        this.triedToFetchSubPages = triedToFetchSubPages;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }
}

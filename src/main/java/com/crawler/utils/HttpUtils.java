package com.crawler.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class HttpUtils {

    public boolean isSameSubDomain(URL url1, URL url2) {
        return url1.getHost().equals(url2.getHost());
    }

    private String getBodyAsString(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }

    public String getHttpResponse(String url) throws IOException {
        HttpGet request = new HttpGet(url);
        HttpResponse response = HttpClientBuilder.create().build().execute(request);
        if (!response.getEntity().getContentType().getValue().contains("text/html")) {
            throw new UnsupportedOperationException("Mime type is not text/html");
        } else {
            return getBodyAsString(response);
        }
    }
}

package com.crawler.utils;

import java.io.*;

public class SaveToDiskUtils {

    public static void saveToDisk(String fileName, String content) {
        try {
            FileWriter fw = new FileWriter(fileName.replaceAll("[^a-zA-Z0-9\\.\\-]", "_"), true);
            fw.write(content);
            fw.flush();
            fw.close();
        } catch (IOException e1) {
            System.out.println("Failed to run crawler");
        }
    }
}

package com.crawler.cache;

import com.crawler.model.Page;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Cache {

    private static final String FILE_NAME = "pages";
    private Map<String, Page> cache = new HashMap<>();

    public Cache() {
        readFromDisk();
        initSaveToDisk();
    }

    private void initSaveToDisk() {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        //todo - can be read from properties file
        scheduler.scheduleAtFixedRate(this::saveToDisk, 10, 1, TimeUnit.MINUTES);
    }

    private void readFromDisk() {
        try {
            FileInputStream fis = new FileInputStream(FILE_NAME);
            final ObjectInputStream ois = new ObjectInputStream(fis);
            final Object deserializedObject = ois.readObject();
            if (deserializedObject instanceof Map)
            {
                cache = (Map<String, Page>) deserializedObject;
            }
        } catch (FileNotFoundException e) {
            // No cache file yet, ignore
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void saveToDisk() {
        try {
            final FileOutputStream fo = new FileOutputStream("pages");
            final ObjectOutputStream oos = new ObjectOutputStream(fo);
            oos.writeObject(cache);
            oos.flush();
            oos.close();
        }
        catch (Exception ex) {
            System.out.println("Failed to save cache to disk");
        }
    }

    public void addToCache(Page page) {
        cache.put(page.getUrl(), page);
    }

    public Optional<Page> getFromCache(String url) {
        if (cache.containsKey(url)) {
            return Optional.of(cache.get(url));
        }
        else return Optional.empty();
    }
}

package com.crawler;

import com.crawler.cache.Cache;
import com.crawler.logic.Crawler;
import com.crawler.utils.HttpUtils;

public class Main {

    public static void main(String[] args) {
        Crawler crawler = new Crawler(new HttpUtils(), new Cache());
        if (validateInput(args)) {
            crawler.crawl(args[0], Integer.parseInt(args[1]));
        }
    }

    private static boolean validateInput(String[] parts) {
        if (parts.length != 2) {
            System.out.println("Please provide input in the format of <url> <depth>");
            return false;
        } else {
            return validateDepth(parts[1]);
        }
    }

    private static boolean validateDepth(String depthStr) {
        try {
            int depth = Integer.parseInt(depthStr);
            if (depth < 1) {
                System.out.println("Please provide depth >= 1");
                return false;
            } else {
                return true;
            }
        } catch (NumberFormatException e) {
            System.out.println("Please provide depth as an integer.");
            return false;
        }
    }
}
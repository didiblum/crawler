package com.crawler.logic;

import com.crawler.cache.Cache;
import com.crawler.model.Page;
import com.crawler.utils.HttpUtils;
import com.crawler.utils.SaveToDiskUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Crawler {

    private HttpUtils httpUtils;
    private Cache cache;
    private static int cacheHit = 0;
    private List<String> pages = new ArrayList<>();

    public Crawler(HttpUtils httpUtils, Cache cache) {
        this.httpUtils = httpUtils;
        this.cache = cache;
    }

    public void crawl(String url, int depth) {
        pages.add(url);
        int currentDepth = 1;
        while (currentDepth < depth + 1 && !pages.isEmpty()) {
            currentDepth = crawlInDepth(currentDepth);
        }
    }

    private int crawlInDepth(int depth) {
        List<String> newPages = new ArrayList<>();
        pages.forEach(urlToFetch -> {
            Page pageWithLinks = getPageWithLinks(urlToFetch);
            printRation(pageWithLinks, depth);
            newPages.addAll(pageWithLinks.getSubPages().stream().map(Page::getUrl).collect(Collectors.toList()));
        });
        pages = newPages;
        return depth + 1;
    }

    private void printRation(Page page, int depth) {
        String s = page.getUrl() + "     " + depth + "    " +
                page.getRatio() + "     \n";
        System.out.println(s);
        SaveToDiskUtils.saveToDisk("output.txt", s);
    }

    private double calculateRatio(String pageUrlStr, List<String> urls) throws MalformedURLException {
        URL pageUrl = new URL(pageUrlStr);
        long sameSub = urls.stream().filter(link -> {
            try {
                return httpUtils.isSameSubDomain(new URL(link), pageUrl);
            } catch (MalformedURLException e) {
                return false;
            }
        }).count();
        return sameSub / (double) urls.size();
    }

    private Page getPageWithLinks(String url) {
        Optional<Page> pageFromCache = cache.getFromCache(url);
        if (pageFromCache.isPresent()) {
            Page page = pageFromCache.get();
            if (page.isTriedToFetchSubPages()) {
                cacheHit++;
                return page;
            }
        }
        return getPageFromWeb(url);
    }

    private Page getPageFromWeb(String url) {
        try {
            String httpBody = geBodyAndSave(url);
            Document doc = Jsoup.parse(httpBody, url);
            Elements staticLinks = doc.select("a[href]");
            Page page = createPage(url, staticLinks);
            cache.addToCache(page);
            return page;
        } catch (Exception e) {
            return createEmptyPage(url);
        }
    }

    private String geBodyAndSave(String url) throws IOException {
        String httpBody = httpUtils.getHttpResponse(url);
        SaveToDiskUtils.saveToDisk(url + ".txt", httpBody);
        return httpBody;
    }

    private Page createEmptyPage(String url) {
        Page page = new Page(url);
        page.setTriedToFetchSubPages(true);
        return page;
    }

    private Page createPage(String url, Elements staticLinks) throws MalformedURLException {
        Page page = new Page(url);
        List<Page> links = new ArrayList<>(staticLinks.size());
        links.addAll(staticLinks.stream().map(
                link -> new Page(link.absUrl("href"))).collect(Collectors.toList()));
        page.setSubPages(links);
        page.setTriedToFetchSubPages(true);
        page.setRatio(calculateRatio(url, links.stream().map(Page::getUrl).collect(Collectors.toList())));
        return page;
    }
}
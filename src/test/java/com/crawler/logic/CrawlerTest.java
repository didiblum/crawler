package com.crawler.logic;

import com.crawler.cache.Cache;
import com.crawler.model.Page;
import com.crawler.utils.HttpUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;

import static org.mockito.Mockito.*;


public class CrawlerTest {

    private HttpUtils httpUtils = mock(HttpUtils.class);
    private Cache cache = mock(Cache.class);

    @Test
    public void crawlWithNoResultsFromCache() {
        try {
            when(cache.getFromCache("test")).thenReturn(Optional.empty());
            Crawler crawlerUnderTest = new Crawler(httpUtils, cache);
            crawlerUnderTest.crawl("test", 1);
            verify(cache, times(1)).getFromCache("test");
            verify(httpUtils, times(1)).getHttpResponse("test");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void crawlWithResultsFromCacheBeforeCrawl() {
        try {
            Page pageBeforeCrawl = new Page("test");
            when(cache.getFromCache("test")).thenReturn(Optional.of(pageBeforeCrawl));
            Crawler crawlerUnderTest = new Crawler(httpUtils, cache);
            crawlerUnderTest.crawl("test", 1);
            verify(cache, times(1)).getFromCache("test");
            verify(httpUtils, times(1)).getHttpResponse("test");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void crawlWithResultsFromCache() {
        try {
            Page page = new Page("test");
            page.setTriedToFetchSubPages(true);
            when(cache.getFromCache("test")).thenReturn(Optional.of(page));
            Crawler crawlerUnderTest = new Crawler(httpUtils, cache);
            crawlerUnderTest.crawl("test", 1);
            verify(cache, times(1)).getFromCache("test");
            verify(httpUtils, never()).getHttpResponse("test");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
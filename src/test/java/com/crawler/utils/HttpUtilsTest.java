package com.crawler.utils;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class HttpUtilsTest {

    @org.junit.Test
    public void isSameSubDomain() {
        try {
            assertTrue(new HttpUtils().isSameSubDomain(
                    new URL("http://www.foo.com/bar.html"),
                    new URL("http://www.foo.com/a.html")));

            assertFalse(new HttpUtils().isSameSubDomain(
                    new URL("http://www.foo.com/bar.html"),
                    new URL("http://baz/foo.com")));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
**Input**

The web crawler should be executed with two arguments: the URL of the root page and the recursion depth limit (a positive integer).

**Output**

The crawler output will be stored in output.txt a TSV (tab separated values) containing three columns:
the URL of the page downloaded, its depth in the links tree and its same domain links ratio.

The crawler output is a TSV (tab separated values) file containing three columns:

1. The URL of the page downloaded

2. The url depth in the links tree 

3. Same domain links ratio


** Output Example: **

https://news.ycombinator.com/     1    0.8495575221238938     
https://news.ycombinator.com     2    0.8495575221238938     
https://news.ycombinator.com/news     2    0.8495575221238938     
https://news.ycombinator.com/newest     2    0.6818181818181818     
https://news.ycombinator.com/newcomments     2    0.8142857142857143     
...
